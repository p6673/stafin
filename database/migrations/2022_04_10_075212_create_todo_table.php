<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('todo', function (Blueprint $table) {
            $table->string('id')->nullable(false);
            $table->string('account')->nullable(false);
            $table->string('title')->nullable(false);
            $table->date('date')->nullable(true);
            $table->time('time')->nullable(true);
            $table->text('desc')->nullable(true);
            $table->boolean('status')->nullable(false)->default(false);
            $table->timestamps();
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('todo');
    }
};
