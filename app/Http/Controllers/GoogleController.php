<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use Exception;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Authenticatable;

class GoogleController extends Controller
{
    public function index()
    {
        return view('login');
    }

    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleGoogleCallback()
    {
        try {
            $user = Socialite::driver('google')->user();
            $finduser = User::where('google_id', $user->id);
            if ($finduser->first()) {
                $finduser->update([
                    'name' => $user->name,
                    'picture' => $user->avatar
                ]);
            } else {
                $newUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'google_id' => $user->id,
                    'picture' => $user->avatar,
                ]);
                $newUser->save();
            }
            session([
                'name' => $user->name,
                'email' => $user->email,
                'id' => $user->id,
                'picture' => $user->avatar,
            ]);
            $this->notification(true, 'Login Successful');
            return redirect()->intended('/');
        } catch (Exception $e) {
            $this->notification(false, 'Bad Request');
            return redirect('login');
        }
    }
}
