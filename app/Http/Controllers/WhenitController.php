<?php

namespace App\Http\Controllers;

use App\Models\VoteModel;
use App\Models\WhenitModel;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class WhenitController extends Controller
{
    public function __construct()
    {
        $this->middleware('login');
    }

    public function index()
    {
        session(['nav' => 'whenit']);
        return view('whenit', ['todayEvent' => WhenitModel::where('account', session('id'))->whereRaw('DATE(summary) = "' . $this->today() . '"')->get(), 'uncompleted' => WhenitModel::where('account', session('id'))->where('status', false)->orderBy('created_at')->get(), 'archived' => WhenitModel::where('account', session('id'))->where('status', 2)->orderBy('created_at')->get(), 'completed' => WhenitModel::where('account', session('id'))->where('status', true)->orderBy('summary')->get(), 'today' => $this->today()]);
    }

    public function store(Request $request)
    {
        try {
            $request->validate([
                'title' => 'required',
                'start' => 'required',
                'end' => 'required',

            ]);
            $whenit = new WhenitModel;
            $whenit->id = $this->randString(64);
            $whenit->account = session('id');
            $whenit->title = $request->title;
            $whenit->start = $request->start;
            $whenit->end = $request->end;
            $whenit->desc = $request->desc;

            $whenit->save();
            $this->notification(true, 'Event Successfully Added');
        } catch (\Throwable $th) {
            $this->notification(false, $th->getMessage());
        }
        return redirect('/whenit');
    }

    public function update(Request $request, WhenitModel $whenit)
    {
        try {
            $validate = $request->validate([
                'title' => "required",
                'start' => "required",
                'end' => "required",
                'desc' => ""
            ]);
            $whenit->update($validate);
            $this->notification(true, 'Event Successfully Updated');
        } catch (\Throwable $th) {
            $this->notification(false, $th->getMessage());
        }
        return redirect('/whenit');
    }

    public function archive(WhenitModel $whenit)
    {
        try {
            $whenit->update(['status' => 2]);
            $this->notification(true, 'Event Successfully Archived');
        } catch (\Throwable $th) {
            $this->notification(false, $th->getMessage());
        }
        return redirect('/whenit');
    }

    public function unarchive(WhenitModel $whenit)
    {
        try {
            $whenit->update(['status' => 1]);
            $this->notification(true, 'Event Successfully Unarchived');
        } catch (\Throwable $th) {
            $this->notification(false, $th->getMessage());
        }
        return redirect('/whenit');
    }

    public function destroy(WhenitModel $whenit)
    {
        try {
            VoteModel::where('event', $whenit->id)->delete();
            $whenit->delete();
            $this->notification(true, 'Event Successfully Deleted');
        } catch (\Throwable $th) {
            $this->notification(false, $th->getMessage());
        }
        return redirect('/whenit');
    }
}
