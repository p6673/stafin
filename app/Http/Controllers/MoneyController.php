<?php

namespace App\Http\Controllers;

use App\Models\BankModel;
use App\Models\WishlistModel;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class MoneyController extends Controller
{
    public function __construct()
    {
        $this->middleware('login');
    }

    public function index()
    {
        session(['nav' => 'money']);
        return view('money', [
            'earn' => BankModel::where('account', session('id'))->where('date', $this->today())->where('type', "in")->sum('amount'),
            'spend' => BankModel::where('account', session('id'))->where('date', $this->today())->where('type', "out")->sum('amount'),
            'transactions' => BankModel::where('account', session('id'))->orderBy('date', 'desc')->get(),
            'today' => $this->today(),
            'wishlist' => WishlistModel::where('account', session('id'))->get(),
            'total' => BankModel::where('account', session('id'))->whereRaw('MONTH(date) = ' . Carbon::now()->month)->sum('amount'),
            'Mearn' => BankModel::where('account', session('id'))->whereRaw('MONTH(date) = ' . Carbon::now()->month)->where('type', "in")->sum('amount'),
            'Mspend' => BankModel::where('account', session('id'))->whereRaw('MONTH(date) = ' . Carbon::now()->month)->where('type', "out")->sum('amount'),
            'totalWl' => WishlistModel::where('account', session('id'))->sum('price'),
        ]);
    }

    public function store(Request $request)
    {
        try {
            $request->validate([
                'amount' => 'required | numeric',
                'type' => 'required',
                'date' => 'required',
            ]);
            $money = new BankModel;
            $money->id = $this->randString(64);
            $money->account = session('id');
            $money->amount = $request->amount;
            $money->desc = $request->desc;
            $money->type = $request->type;
            $money->date = $request->date;

            $money->save();
            session()->flash('notification', true);
            session()->flash('status', true);
            session()->flash('msg', 'Transaction Successfully Added');
        } catch (\Throwable $th) {
            session()->flash('notification', true);
            session()->flash('status', false);
            session()->flash('msg', $th->getMessage());
        }
        return redirect('/money');
    }

    public function update(Request $request, BankModel $money)
    {
        try {
            $validate = $request->validate([
                'amount'           => 'required | numeric',
                'type'           => 'required',
                'date'           => 'required',
                'desc'           => '',
            ]);
            $money->update($validate);
            $this->notification(true, 'Transaction Successfully Updated');
        } catch (\Throwable $th) {
            $this->notification(false, $th->getMessage());
        }
        return redirect('/money');
    }

    public function destroy(BankModel $money)
    {
        try {
            $money->delete();
            $this->notification(true, 'Transaction Successfully Deleted');
        } catch (\Throwable $th) {
            $this->notification(false, $th->getMessage());
        }
        return redirect('/money');
    }
}
