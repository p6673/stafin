<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Str;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function randString($length)
    {
        return Str::random($length);
    }

    public function today()
    {
        return Carbon::now()->toDateString();
    }

    public function notification($status, $msg)
    {
        session()->flash('notification', true);
        session()->flash('status', $status);
        session()->flash('msg', $msg);
    }
}
