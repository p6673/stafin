<?php

namespace App\Http\Controllers;

use App\Models\BankModel;
use App\Models\TodoModel;
use App\Models\WhenitModel;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('login');
    }

    public function index()
    {
        session(['nav' => 'dashboard']);
        $total = BankModel::where('account', session('id'))->where('date', $this->today())->sum('amount');
        $earn = BankModel::where('account', session('id'))->where('date', $this->today())->where('type', "in")->sum('amount');
        $spend = BankModel::where('account', session('id'))->where('date', $this->today())->where('type', "out")->sum('amount');
        return view('dashboard', ['earn' => $earn, 'total' => $total, 'spend' => $spend, 'transactions' => BankModel::where('account', session('id'))->orderBy('date', 'desc')->limit(3)->get(), 'todayTask' => TodoModel::where('account', session('id'))->where('date', $this->today())->where('status', false)->orderBy('time')->get(), 'upcoming' => TodoModel::where('account', session('id'))->where('date', '>', $this->today())->where('status', false)->orderBy('date')->get(), 'completed' => WhenitModel::where('account', session('id'))->where('status', true)->orderBy('summary')->get(), 'today' => $this->today()]);
    }
}
