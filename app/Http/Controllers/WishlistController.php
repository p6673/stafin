<?php

namespace App\Http\Controllers;

use App\Models\WishlistModel;
use Illuminate\Http\Request;

class WishlistController extends Controller
{
    public function __construct()
    {
        $this->middleware('login');
    }
    public function index()
    {
        return redirect('/money');
    }

    public function store(Request $request)
    {
        try {
            $request->validate([
                'item' => 'required',
                'price' => 'required | numeric',
            ]);
            $wishlist = new WishlistModel;
            $wishlist->id = $this->randString(64);
            $wishlist->account = session('id');
            $wishlist->item = $request->item;
            $wishlist->price = $request->price;

            $wishlist->save();
            $this->notification(true, 'Transaction Successfully Added');
        } catch (\Throwable $th) {
            $this->notification(false, $th->getMessage());
        }
        return redirect('/money');
    }

    public function update(Request $request, WishlistModel $wishlist)
    {
        try {
            $validateData = $request->validate([
                'item'           => 'required',
                'price'          => 'required',
            ]);

            $wishlist->update($validateData);
            $this->notification(true, 'Transaction Successfully Updated');
        } catch (\Throwable $th) {
            $this->notification(false, $th->getMessage());
        }
        return redirect('/money');
    }

    public function destroy(WishlistModel $wishlist)
    {
        try {
            $wishlist->delete();
            $this->notification(true, 'Transaction Successfully Deleted');
        } catch (\Throwable $th) {
            $this->notification(false, $th->getMessage());
        }
        return redirect('/wishlist');
    }
}
