<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\TodoModel;
use App\Models\WhenitModel;

class TodoController extends Controller
{
    public function __construct()
    {
        $this->middleware('login');
    }

    public function index()
    {
        session(['nav' => 'todo']);
        return view('todo', ['todo' => TodoModel::where('account', session('id'))->where('status', false)->orderBy('date')->get(), 'completed' => TodoModel::where('account', session('id'))->where('status', true)->orderBy('date', 'desc')->get(), 'todayTask' => TodoModel::where('account', session('id'))->where('date', $this->today())->where('status', false)->orderBy('time')->get(), 'today' => $this->today()]);
    }

    public function store(Request $request)
    {
        try {
            $request->validate([
                'title' => 'required',
            ]);
            $todo = new TodoModel;
            $todo->id = $this->randString(64);
            $todo->account = session('id');
            $todo->title = $request->title;
            $todo->date = $request->date;
            $todo->time = $request->time;
            $todo->desc = $request->desc;

            $todo->save();
            $this->notification(true, 'Task Successfully Added');
        } catch (\Throwable $th) {
            $this->notification(false, $th->getMessage());
        }
        return redirect('/todo');
    }

    public function update(Request $request, TodoModel $todo)
    {
        try {
            $validate = $request->validate([
                'title' => "required",
                'date' => "",
                'time' => "",
                'desc' => ""
            ]);
            $todo->update($validate);
            $this->notification(true, 'Task Successfully Updated');
        } catch (\Throwable $th) {
            $this->notification(false, $th->getMessage());
        }
        return redirect('/todo');
    }

    public function check(TodoModel $todo)
    {
        try {
            $todo->status = 1;
            $todo->update();
            $this->notification(true, 'Task Successfully Checked');
        } catch (\Throwable $th) {
            $this->notification(false, $th->getMessage());
        }
        return redirect('/todo');
    }

    public function uncheck(TodoModel $todo)
    {
        try {
            $todo->status = 0;
            $todo->update();
            $this->notification(true, 'Task Successfully Unchecked');
        } catch (\Throwable $th) {
            $this->notification(false, $th->getMessage());
        }
        return redirect('/todo');
    }

    public function destroy(TodoModel $todo)
    {
        try {
            $todo->delete();
            $this->notification(true, 'Task Successfully Deleted');
        } catch (\Throwable $th) {
            $this->notification(false, $th->getMessage());
        }
        return redirect('/todo');
    }
}
