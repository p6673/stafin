<?php

namespace App\Http\Controllers;

use App\Models\VoteModel;
use App\Models\WhenitModel;
use Illuminate\Http\Request;

class VoteController extends Controller
{
    public function index($id)
    {
        $total = 0;
        if (VoteModel::where('event', $id)->count() > 0) $total = VoteModel::where('event', $id)->selectRaw('inputId, name, sum(TIME_TO_SEC(end)/3600 - TIME_TO_SEC(start)/3600) as total')->groupBy('inputId', 'name')->orderBy('total', 'desc')->first()->total;
        $summary = $this->summary(WhenitModel::where('id', $id)->first());
        $event = VoteModel::where('event', $id)->selectRaw('vote, avg(TIME_TO_SEC(end)/3600 - TIME_TO_SEC(start)/3600) as dur, count(vote) as voted')->where('visible', 1)->groupBy('vote')->orderBy('vote')->get();
        $participans = VoteModel::where('event', $id)->selectRaw('inputId, name, sum(TIME_TO_SEC(end)/3600 - TIME_TO_SEC(start)/3600) as total')->where('visible', 1)->groupBy('inputId', 'name')->orderBy('total', 'desc')->get();
        $hided = VoteModel::where('event', $id)->selectRaw('inputId, name, sum(TIME_TO_SEC(end)/3600 - TIME_TO_SEC(start)/3600) as total')->where('visible', 0)->groupBy('inputId', 'name')->orderBy('total', 'desc')->get();
        return view('vote.index', ['event' => $event, 'summary' => $summary, 'participans' => $participans, 'hided' => $hided, 'id' => WhenitModel::where('id', $id)->first(), 'max' => $total]);
    }

    public function form(WhenitModel $id)
    {
        if (!$id->status) {
            $id->summary = array($id->start);
            while ($id->start != $id->end) {
                $id->start = date("Y-m-d", date_format(date_create($id->start), "U") + 86400);
                $id->summary = array_merge($id->summary, array($id->start));
            }
        }
        return view('vote.form', ['id' => $id]);
    }

    public function store(Request $request, WhenitModel $id)
    {
        if (!$id->status) {
            try {
                $request->validate(['name' => 'required']);
                $inputId = $this->randString(64);
                for ($a = 0; $a <= (date_format(date_create($id->end), "U") - date_format(date_create($id->start), "U")) / 86400; $a++) {
                    if ($request['start' . $a] == '' || $request['end' . $a] == '') continue;
                    $vote = new VoteModel();
                    $vote->id = $this->randString(64);
                    $vote->event = $id->id;
                    $vote->inputId = $inputId;
                    $vote->name = $request->name;
                    $vote->vote = date('Y-m-d', date_format(date_create($id->start), "U") + (86400 * $a));
                    $vote->start = $request['start' . $a];
                    $vote->end = $request['end' . $a];
                    $vote->save();
                }
                $this->notification(true, 'Vote Successfully Added');
            } catch (\Throwable $th) {
                $this->notification(false, $th->getMessage());
            }
            return redirect('/vote/' . $id->id . '/form');
        } else {
            return redirect('/vote/' . $id->id . '/form');
        }
    }

    public function summary(WhenitModel $id)
    {
        $res = array();
        for ($a = 0; $a <= (date_format(date_create($id->end), "U") - date_format(date_create($id->start), "U")) / 86400; $a++) {
            $date = date("Y-m-d", date_format(date_create($id->start), "U") + ($a * 86400));
            if (VoteModel::where('event', $id->id)->where('vote', $date)->where('visible', 1)->count() == 0) continue;

            $minH = VoteModel::where('event', $id->id)->where('vote', $date)->where('visible', 1)->selectRaw('min(HOUR(start)) as min')->first()->min;
            $maxH = VoteModel::where('event', $id->id)->where('vote', $date)->where('visible', 1)->selectRaw('max(HOUR(end)) as max')->first()->max;
            $hours = array();
            for (; $minH <= $maxH; $minH++) {
                $hours[$minH] = VoteModel::where('event', $id->id)->where('vote', $date)->where('visible', 1)->whereRaw('HOUR(start)<=' . $minH . ' AND HOUR(end) >=' . $minH)->count();
            }

            $max = max($hours);
            $res[$date]['start'] = null;
            $res[$date]['end'] = null;
            foreach ($hours as $key => $h) {
                if ($h == $max && $res[$date]['start'] == null) $res[$date]['start'] = $key . '';
                if (isset($hours[$key + 1])) {
                    if ($h == $max && $hours[$key + 1] < $max) {
                        $res[$date]['end'] = $key + 1 . ':00';
                        break;
                    }
                } else {
                    if ($h == $max) {
                        $res[$date]['end'] = $key + 1 . ':00';
                        break;
                    }
                }
            }
            if (VoteModel::where('event', $id->id)->where('vote', $date)->where('visible', 1)->where('start', '<=', $res[$date]['start'] . ':30')->count() < VoteModel::where('event', $id->id)->where('vote', $date)->where('visible', 1)->where('start', '>', $res[$date]['start'] . ':30')->count()) $res[$date]['start'] = $res[$date]['start'] . ':00';
            else $res[$date]['start'] = $res[$date]['start'] . ':30';
        }
        return $res;
    }

    public function hide($id, $hide)
    {
        try {
            VoteModel::where('inputId', $hide)->update(['visible' => 0]);
            $this->notification(true, 'Participan Successfully Hided');
        } catch (\Throwable $th) {
            $this->notification(false, $th->getMessage());
        }
        return redirect('/vote/' . $id);
    }

    public function show($id, $show)
    {
        try {
            VoteModel::where('inputId', $show)->update(['visible' => 1]);
            $this->notification(true, 'Participan Successfully Showed');
        } catch (\Throwable $th) {
            $this->notification(false, $th->getMessage());
        }
        return redirect('/vote/' . $id);
    }

    public function setSummary(WhenitModel $id, $date, $time)
    {
        try {
            $id->summary = $date . ' ' . $time;
            $id->status = 1;
            $id->update();
            $this->notification(true, 'Summary Successfully Selected');
        } catch (\Throwable $th) {
            $this->notification(false, $th->getMessage());
        }
        return redirect('/vote/' . $id->id);
    }
}
