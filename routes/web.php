<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\GoogleController;
use App\Http\Controllers\MoneyController;
use App\Http\Controllers\TodoController;
use App\Http\Controllers\VoteController;
use App\Http\Controllers\WhenitController;
use App\Http\Controllers\WishlistController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware('guest')->group(function () {
    Route::get('/login', [GoogleController::class, 'index']);
    Route::get('auth/google', [GoogleController::class, 'redirectToGoogle']);
    Route::get('auth/google/callback', [GoogleController::class, 'handleGoogleCallback']);
});
Route::get('/vote/{id}/form', [VoteController::class, "form"]);
Route::post('/vote/{id}/form', [VoteController::class, "store"]);

Route::middleware('login')->group(function () {
    Route::resource('/', DashboardController::class);
    Route::resource('/money', MoneyController::class);
    Route::resource('/todo', TodoController::class);
    Route::patch('/todo/{todo}/check', [TodoController::class, 'check']);
    Route::patch('/todo/{todo}/uncheck', [TodoController::class, 'uncheck']);
    Route::resource('/whenit', WhenitController::class);
    Route::patch('/whenit/{whenit}/archive', [WhenitController::class, 'archive']);
    Route::patch('/whenit/{whenit}/unarchive', [WhenitController::class, 'unarchive']);
    Route::resource('/wishlist', WishlistController::class);

    Route::get('/vote/{id}', [VoteController::class, "index"]);
    Route::get('/vote/{id}/summary', [VoteController::class, "summary"]);
    Route::get('/vote/{id}/summary/{date}/{time}', [VoteController::class, "setSummary"]);
    Route::get('/vote/{id}/hide/{hide}', [VoteController::class, "hide"]);
    Route::get('/vote/{id}/show/{show}', [VoteController::class, "show"]);
    Route::get('/logout', function () {
        session()->flush();
        return redirect('/login');
    });
});
