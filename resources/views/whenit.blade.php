@extends('layouts.template')

@section('tabTitle', 'When It')
@section('title', 'When It')

@section('left')
@endsection

@section('left1')
    <x-card :shadow="true" style="primary">
        <h2 class="ps-4 py-2 mb-4 fs-4 wht9 f600">New Event</h2>
        <form action="/whenit" method="POST">
            @csrf
            <input type="text" style="outline:none" class="border-0 r20 bg2 px-4 py-3 me-4 fs-5 wht9 f600 w-100"
                placeholder="Title" name="title" required>
            <div class="row mt-4">
                <div class="col-12 col-lg-6 d-flex">
                    <div class="r20 d-flex bg2 py-3 ps-4 pe-3 w-100">
                        <label for="inpstart" class="f600 wht6 fs-6 align-self-center">From</label>
                        <input type="date" id="inpstart" class="f600 bg2 border-0 wht6 py-1 text-end w-100"
                            style="outline: none" placeholder="Start" name="start" required>
                    </div>
                </div>
                <div class="col-12 col-lg-6 d-flex mt-4 mt-lg-0">
                    <div class="r20 d-flex bg2 py-3 px-4 w-100">
                        <label for="inpend" class="f600 wht6 fs-6 align-self-center">to</label>
                        <input type="date" id="inpend" class="f600 bg2 border-0 wht6 py-1 text-end w-100"
                            style="outline: none" placeholder="End" name="end" required>
                    </div>
                </div>
            </div>
            <textarea class="border-0 bg2 r20 my-4 p-4 fs-6 wht8 f600 w-100" style="outline:none" rows="3" placeholder="Description"
                name="desc"></textarea>
            <div class="d-flex mb-3">
                <button type="submit" class="f600 d-none d-md-block shdw bg2 border-0 r20 wht9 px-5 py-3 ">Add
                    Event</button>
                <button type="submit" class="f600 d-md-none d-block shdw bg2 border-0 r20 wht9 px-5 py-3 ">Add</button>
                <button type="reset" class="f600 bg-transparent border-0 r20 wht6 px-4 py-3 ">Clear</button>
            </div>
        </form>
    </x-card>
@endsection

@section('right')
    <h2 class="ps-4 py-2 mb-4 mt-5 mt-lg-2 fs-4 wht9 f600">Today's Events</h2>
    @forelse ($todayEvent as $ev)
        <a href="/vote/{{ $ev->id }}" class="text-decoration-none">
            <x-card class="me-3 mb-5" :shadow="true" style="secondary">
                <div class="px-3 py-4">
                    <div class="d-flex">
                        <div class="align-self-center rounded-circle me-3"
                            style="width:1rem; height:1rem; border:3px solid var(--cl0)">
                        </div>
                        <p class="align-self-center m-0 f600 fs-7 wht6">
                            Vote Completed
                        </p>
                    </div>
                    <div class="pt-3">
                        <p class="fs-5 cl0 f600 mb-0">
                            {{ $ev->title }}
                        </p>
                        <p class="fs-7 wht6 f600 text">
                            {{ $ev->desc }}
                        </p>
                        <div class="d-flex justify-content-end">
                            <p class="mb-0 fs-7 cl0 f600 ">
                                {{ $ev->summary }}
                            </p>
                        </div>
                    </div>
                </div>
            </x-card>
        </a>
    @empty
        <x-card class="mb-5" :shadow="true" style="secondary">
            <h2 class="py-5 text-center m-0 fs-6 wht9 f600">No Events Today</h2>
        </x-card>
    @endforelse
@endsection

@section('bottom')
    <section class="pt-5 ">
        <h2 class="text-center fs-4 wht9 f600">Event List</h2>
        <section class="row px-3 pt-4 pb-5">
            <p class="wht8 fs-6 f600 ps-5 mb-1 ms-2">Vote Completed</p>
            @forelse ($completed as $ev)
                <div class="col-12 col-md-6 col-lg-4">
                    <a href="/vote/{{ $ev->id }}" class="text-decoration-none">
                        <x-card class="me-3 mb-4" :shadow="true" style="secondary">
                            <div class="px-3 py-4">
                                <div class="d-flex">
                                    <div class="align-self-center rounded-circle me-3"
                                        style="width:1rem; height:1rem; border:3px solid var(--cl0)">
                                    </div>
                                    <p class="align-self-center m-0 f600 fs-7 wht6">
                                        Vote Completed
                                    </p>
                                </div>
                                <div class="pt-3">
                                    <p class="fs-5 cl0 f600 mb-0">
                                        {{ $ev->title }}
                                    </p>
                                    <p class="fs-7 wht6 f600 text">
                                        {{ $ev->desc }}
                                    </p>
                                    <div class="d-flex justify-content-end">
                                        <p class="mb-0 fs-7 @if (explode(' ', $ev->summary)[0] == $today) cl0 @else wht9 @endif f600 ">
                                            {{ $ev->summary }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </x-card>
                    </a>
                </div>
            @empty
                <x-card class="mb-5" :shadow="true" style="secondary">
                    <h2 class="py-5 text-center m-0 fs-6 wht9 f600">No Events</h2>
                </x-card>
            @endforelse
        </section>

        <section class="row px-3 py-5">
            <p class="wht8 fs-6 f600 ps-5 mb-1 ms-2">Vote Uncompleted</p>
            @forelse ($uncompleted as $ev)
                <div class="col-12 col-md-6 col-lg-4">
                    <div data-bs-toggle="modal" data-bs-target="#mainModal"
                        onclick="setData({{ Illuminate\Support\Js::from($ev) }})">
                        <x-card class="me-3 mb-4" :shadow="true" style="secondary">
                            <div class="px-3 py-4">
                                <div class="d-flex">
                                    <div class="align-self-center rounded-circle me-3"
                                        style="width:1rem; height:1rem; border:3px solid var(--cl1)">
                                    </div>
                                    <p class="align-self-center m-0 f600 fs-7 wht6">
                                        Vote Uncompleted
                                    </p>
                                </div>
                                <div class="pt-3">
                                    <p class="fs-5 cl1 f600 mb-0">
                                        {{ $ev->title }}
                                    </p>
                                    <p class="fs-7 wht6 f600 text">
                                        {{ $ev->desc }}
                                    </p>
                                </div>
                            </div>
                        </x-card>
                    </div>
                </div>
            @empty
                <x-card class="mb-5" :shadow="true" style="secondary">
                    <h2 class="py-5 text-center m-0 fs-6 wht9 f600">No Events</h2>
                </x-card>
            @endforelse
        </section>

        <section class="row px-3 py-5">
            <p class="wht8 fs-6 f600 ps-5 mb-1 ms-2">Archived Events</p>
            @forelse ($archived as $ev)
                <div class="col-12 col-md-6 col-lg-4">
                    <a href="/vote/{{ $ev->id }}" class="text-decoration-none">
                        <x-card class="me-3 mb-5" :shadow="true" style="secondary">
                            <div class="p-3">
                                <div class="d-flex">
                                    <div class="align-self-center rounded-circle me-3"
                                        style="width:1rem; height:1rem; border:3px solid var(--cl0)">
                                    </div>
                                    <p class="align-self-center m-0 f600 fs-7 wht6">
                                        Vote Completed
                                    </p>
                                </div>
                                <div class="pt-3">
                                    <p class="fs-6 wht6 f600 mb-0">
                                        {{ $ev->title }}
                                    </p>
                                    <p class="fs-7 wht6 f600 text">
                                        {{ $ev->desc }}
                                    </p>
                                    <div class="d-flex justify-content-end">
                                        <p class="mb-0 fs-7 wht6 f600 ">
                                            {{ $ev->summary }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </x-card>
                    </a>
                </div>
            @empty
                <x-card class="mb-5" :shadow="true" style="secondary">
                    <h2 class="py-5 text-center m-0 fs-6 wht9 f600">No Archives</h2>
                </x-card>
            @endforelse
        </section>

    </section>
@endsection

@section('modal')
    <div class="modal fade" id="mainModal" tabindex="-1" aria-labelledby="mainModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="background:transparent;">
                <x-card :shadow="true" style="primary">
                    <div class="d-flex justify-content-between mb-4">
                        <div class="d-flex">
                            <h5 class="ps-4 py-2 fs-5 wht9 f600 modal-title">Event Detail</h5>
                            <button type="button" class="btn cl0"
                                onclick="return window.prompt('Copy to clipboard: Ctrl+C, Enter', linkShare);"><i
                                    class="bi bi-clipboard"></i></button>
                        </div>
                        <form action="/whenit/destroy" id="deleteForm" method="POST">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="align-self-center btn m-0 p-2 cl1 f600 fs-6"
                                onclick="return confirm('Are you sure to delete this event ?')">Delete</button>
                        </form>
                    </div>
                    <form action="/whenit" id="updateForm" method="POST">
                        @csrf
                        @method('PATCH')
                        <input type="text" style="outline:none" class="border-0 r20 bg2 px-4 py-3 me-4 fs-5 wht9 f600 w-100"
                            placeholder="Title" name="title" id="vTitle" required>
                        <div class="row mt-4">
                            <div class="col-12 col-lg-6 d-flex">
                                <div class="r20 d-flex bg2 py-3 ps-4 pe-3 w-100">
                                    <label for="vStart" class="f600 wht6 fs-6 align-self-center">From</label>
                                    <input type="date" id="vStart" class="f600 bg2 border-0 wht6 py-1 text-end w-100"
                                        style="outline: none" placeholder="Start" name="start" required>
                                </div>
                            </div>
                            <div class="col-12 col-lg-6 d-flex mt-4 mt-lg-0">
                                <div class="r20 d-flex bg2 py-3 px-4 w-100">
                                    <label for="vEnd" class="f600 wht6 fs-6 align-self-center">to</label>
                                    <input type="date" id="vEnd" class="f600 bg2 border-0 wht6 py-1 text-end w-100"
                                        style="outline: none" placeholder="End" name="end" required>
                                </div>
                            </div>
                        </div>
                        <textarea class="border-0 bg2 r20 my-4 p-4 fs-6 wht8 f600 w-100" style="outline:none" rows="3" placeholder="Description"
                            name="desc" id="vDesc"></textarea>
                        <a href="/vote/" id="voteDetail"
                            class="f600 align-self-center w-100 bg2 mb-4 py-3 border-0 r20 wht9 px-4 btn">Vote
                            Detail</a>
                        <div class="d-flex">
                            <button type="submit"
                                class="f600 d-none d-md-block shdw bg2 border-0 r20 wht9 px-5 py-3 ">Update
                                Event</button>
                            <button type="submit"
                                class="f600 d-md-none d-block shdw bg2 border-0 r20 wht9 px-4 py-3 ">Update
                            </button>
                            <button type="button" class="f600 bg-transparent border-0 r20 wht6 px-4 py-3"
                                data-bs-dismiss="modal">Cancel</button>
                        </div>
                    </form>
                </x-card>
            </div>
        </div>
    </div>
    <script>
        let linkShare = "{{ url('/') }}/vote/";
        const setData = (data) => {
            linkShare += data.id + '/form';
            $('#vTitle').val(data.title);
            $('#vStatus').val(data.status);
            $('#vStart').val(data.start);
            $('#vEnd').val(data.end);
            $('#vSummary').val(data.summary);
            $('#vDesc').val(data.desc);
            $("#updateForm").attr('action', '/whenit/' + data.id);
            $("#deleteForm").attr('action', '/whenit/' + data.id);
            $("#voteDetail").attr('href', '/vote/' + data.id);
        }
    </script>
@endsection
