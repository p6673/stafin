@extends('layouts.template')

@section('tabTitle', 'Dashboard')
@section('title', 'Hello ' . session('name'))

@section('left')
    <section>
        <div class="d-flex">
            <img src="{{ asset('img/in.png') }}" alt="">
            <div class="ps-4 w-100">
                <div class="d-flex justify-content-between">
                    <p class="f600 fs-6 wht9">Earnings</p>
                    <H6 class="f600 fs-5 wht9">@rupiah($earn)</H6>
                </div>
                @if ($total == 0)
                    <x-progress color="#92DCFE" :percentage="0"></x-progress>
                @else
                    <x-progress color="#92DCFE" :percentage="($earn / $total) * 100"></x-progress>
                @endif
            </div>
        </div>
        <br>
        <div class="d-flex">
            <img src="{{ asset('img/out.png') }}" alt="">
            <div class="ps-4 w-100">
                <div class="d-flex justify-content-between">
                    <p class="f600 fs-6 wht9">Spendings</p>
                    <H6 class="f600 fs-5 wht9">@rupiah($spend)</H6>
                </div>
                @if ($total == 0)
                    <x-progress color="#ED5333" :percentage="0"></x-progress>
                @else
                    <x-progress color="#ED5333" :percentage="($spend / $total) * 100"></x-progress>
                @endif
            </div>
        </div>
    </section>
@endsection

@section('left1')
    <x-card :shadow="true" style="primary" class="mt-5">
        <h2 class="ps-4 py-2 mb-4 fs-4 wht9 f600">Last Transaction</h2>
        @forelse ($transactions as $tr)
            <div data-bs-toggle="modal" data-bs-target="#moneyModal"
                onclick="setData({{ Illuminate\Support\Js::from($tr) }})">
                <x-card :shadow="true" style="secondary" class="my-4">
                    <div class="px-3 row">
                        <div class="col-7 d-flex">
                            <span
                                class="align-self-center bi @if ($tr->type == 'in') bi-arrow-down-circle cl0 @endif  @if ($tr->type == 'out') bi-arrow-up-circle cl1 @endif fs-2"></span>
                            <div class="align-self-center ps-3">
                                <p class="f600 fs-6 wht9 mb-0 clamp1">{{ $tr->desc }}</p>
                                <p class="f600 fs-7 wht6 mb-0 lt0">{{ $tr->date }}</p>
                            </div>
                        </div>
                        <div class="col-5 align-self-center text-end">
                            <p class="m-0 f600 fs-5 wht9">@rupiah($tr->amount)</p>
                        </div>
                    </div>
                </x-card>
            </div>
        @empty
            <x-card class="mb-5" :shadow="true" style="secondary">
                <h2 class="py-5 text-center m-0 fs-6 wht9 f600">No Transactions</h2>
            </x-card>
        @endforelse
    </x-card>
@endsection

@section('right')
    <x-card :shadow="true" style="primary">
        <h2 class="ps-4 py-2 mb-4 fs-4 wht9 f600">Today's Task</h2>
        <section>
            @forelse ($todayTask as $tdTask)
                <x-card :shadow="true" style="secondary">
                    <div class="d-flex justify-content-between py-2">
                        <div class="d-flex">
                            <div class="align-self-center rounded-circle me-3"
                                style="width:1.25rem; height:1.25rem; border:3px solid #ED5333"></div>
                            <p class="align-self-center m-0 f600 fs-5 wht9">{{ $tdTask->title }}</p>
                        </div>
                        <p class="align-self-center m-0 f600 fs-6 wht8">{{ $tdTask->time }}</p>
                    </div>
                </x-card>
            @empty
                <x-card class="mb-5" :shadow="true" style="secondary">
                    <h2 class="py-5 text-center m-0 fs-6 wht9 f600">No Tasks Today</h2>
                </x-card>
            @endforelse
        </section>
        <p class="ps-4 pt-3 pb-0 fs-6 wht8 f600">Upcoming</p>
        <section>
            @forelse ($upcoming as $upc)
                <x-card :shadow="false" style="secondary">
                    <div class="d-flex justify-content-between p-1">
                        <p class="align-self-center m-0 f600 fs-6 wht6">{{ $upc->title }}</p>
                        <p class="align-self-center m-0 f600 fs-6 wht6">{{ $upc->date }}</p>
                    </div>
                </x-card>
            @empty
                <x-card class="mb-5" :shadow="true" style="secondary">
                    <h2 class="py-5 text-center m-0 fs-6 wht9 f600">No Upcoming Tasks </h2>
                </x-card>
            @endforelse
        </section>
    </x-card>
@endsection

@section('bottom')
    <x-card :shadow="true" style="primary">
        <h2 class="ps-4 ms-3 py-2 mb-2 fs-4 wht9 f600">Upcoming Event</h2>
        <section class="d-flex" style="overflow-x:scroll; scrolbar">
            @forelse ($completed as $ev)
                <div class="col-12 col-md-6 col-lg-4">
                    <a href="/vote/{{ $ev->id }}" class="text-decoration-none">
                        <x-card class="me-3 mb-4" :shadow="true" style="secondary">
                            <div class="px-3 py-4">
                                <div class="d-flex">
                                    <div class="align-self-center rounded-circle me-3"
                                        style="width:1rem; height:1rem; border:3px solid var(--cl0)">
                                    </div>
                                    <p class="align-self-center m-0 f600 fs-7 wht6">
                                        Vote Completed
                                    </p>
                                </div>
                                <div class="pt-3">
                                    <p class="fs-5 cl0 f600 mb-0">
                                        {{ $ev->title }}
                                    </p>
                                    <p class="fs-7 wht6 f600 text">
                                        {{ $ev->desc }}
                                    </p>
                                    <div class="d-flex justify-content-end">
                                        <p class="mb-0 fs-7 @if (explode(' ', $ev->summary)[0] == $today) cl0 @else wht9 @endif f600 ">
                                            {{ $ev->summary }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </x-card>
                    </a>
                </div>
            @empty
                <x-card class="" :shadow="true" style="secondary">
                    <h2 class="p-5 mx-5 my-4 text-center m-0 fs-5 wht9 f600">No Events</h2>
                </x-card>
            @endforelse
        </section>
    </x-card>
@endsection
{{-- @section('bottom')
    <x-card :shadow="true" style="primary">
        <h2 class="ps-4 ms-3 py-2 mb-2 fs-4 wht9 f600">Upcoming Event</h2>
        <section class="d-flex" style="overflow-x:scroll; scrolbar">
            <x-card class="me-4" :shadow="true" style="secondary">
                <div class="px-3 py-4">
                    <div class="d-flex">
                        <div class="align-self-center rounded-circle me-3"
                            style="width:1rem; height:1rem; border:3px solid #ED5333"></div>
                        <p class="align-self-center m-0 f600 fs-7 wht6">Vote Uncompleted</p>
                    </div>
                    <div class="pt-3" style="width: 300px">
                        <p class="fs-5 wht9 f600 mb-0">
                            Class Meeting
                        </p>
                        <p class="fs-7 wht6 f600 clamp3">
                            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Veritatis dolorum id
                            dignissimos quod voluptatem, quam repellendus magni nemo. Accusantium ex
                            reiciendis
                            voluptatem nemo dicta molestiae ut iure nihil omnis cumque!
                        </p>
                        <div class="d-flex justify-content-end">
                            <p class="mb-0 fs-7 wht9 f600 ">24 Mei 2022 at 10.20</p>
                        </div>
                    </div>
                </div>
            </x-card>
            <x-card class="me-4" :shadow="true" style="secondary">
                <div class="px-3 py-4">
                    <div class="d-flex">
                        <div class="align-self-center rounded-circle me-3"
                            style="width:1rem; height:1rem; border:3px solid #ED5333"></div>
                        <p class="align-self-center m-0 f600 fs-7 wht6">Vote Uncompleted</p>
                    </div>
                    <div class="pt-3" style="width: 300px">
                        <p class="fs-5 wht9 f600 mb-0">
                            Class Meeting
                        </p>
                        <p class="fs-7 wht6 f600 clamp3">
                            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Veritatis dolorum id
                            dignissimos quod voluptatem, quam repellendus magni nemo. Accusantium ex
                            reiciendis
                            voluptatem nemo dicta molestiae ut iure nihil omnis cumque!
                        </p>
                        <div class="d-flex justify-content-end">
                            <p class="mb-0 fs-7 wht9 f600 ">24 Mei 2022 at 10.20</p>
                        </div>
                    </div>
                </div>
            </x-card>
            <x-card class="me-4" :shadow="true" style="secondary">
                <div class="px-3 py-4">
                    <div class="d-flex">
                        <div class="align-self-center rounded-circle me-3"
                            style="width:1rem; height:1rem; border:3px solid #ED5333"></div>
                        <p class="align-self-center m-0 f600 fs-7 wht6">Vote Uncompleted</p>
                    </div>
                    <div class="pt-3" style="width: 300px">
                        <p class="fs-5 wht9 f600 mb-0">
                            Class Meeting
                        </p>
                        <p class="fs-7 wht6 f600 clamp3">
                            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Veritatis dolorum id
                            dignissimos quod voluptatem, quam repellendus magni nemo. Accusantium ex
                            reiciendis
                            voluptatem nemo dicta molestiae ut iure nihil omnis cumque!
                        </p>
                        <div class="d-flex justify-content-end">
                            <p class="mb-0 fs-7 wht9 f600 ">24 Mei 2022 at 10.20</p>
                        </div>
                    </div>
                </div>
            </x-card>
        </section>
    </x-card>
@endsection --}}
