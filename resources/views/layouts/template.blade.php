<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="{{ asset('js/bootstrap.bundle.js') }}"></script>
    <title>@yield('tabTitle')</title>
    <link rel="stylesheet" href="{{ asset('css/all.css') }}">
    <link rel="icon" type="image/x-icon" href="{{ asset('icon/favicon.png') }}">
</head>

<body class="bg0 px-3 pt-3 pb-5">
    <nav class="shdw d-none bg1 position-fixed w-auto pb-4 pt-5 d-lg-flex flex-column justify-content-between"
        style="height: 100vh; top: 0; left: 0;">
        <div class="d-flex flex-column px-4 text-center">
            <a class="py-2 d-flex justify-content-center mb-4" href="/"><img src="{{ asset('icon/icon.png') }}"
                    style="width: 2.3rem;" alt=""></a>
            <a class="py-2 bi bi-columns-gap fs-4 @if (session('nav') == 'dashboard') cl0 @else cl2 @endif" href="/"></a>
            <a class="py-2 bi bi-wallet fs-4 @if (session('nav') == 'money') cl0 @else cl2 @endif" href="/money"></a>
            <a class="py-2 bi bi-card-list fs-4 @if (session('nav') == 'todo') cl0 @else cl2 @endif" href="/todo"></a>
            <a class="py-2 bi bi-calendar-date fs-4 @if (session('nav') == 'whenit') cl0 @else cl2 @endif"
                href="/whenit"></a>
        </div>
        <div class="d-flex flex-column px-1 text-center">
            <a class="py-2" href="/"><img src="{{ session('picture') }}" class="rounded-circle"
                    style="width: 2.3rem" alt=""></a>
            <a class="py-2" href="/logout"><span class="align-self-center bi bi-box-arrow-left fs-4 cl2"
                    style="margin-left: -.7rem" onclick="return confirm('Are you sure to Logout?')"></span></a>
        </div>
    </nav>
    <nav class="shdw d-flex bg1 position-fixed px-3 py-2 d-lg-none justify-content-evenly"
        style="width: 100vw; bottom: 0; left: 0;">
        <a href="/"
            class="py-2 btn bi bi-columns-gap @if (session('nav') == 'dashboard') cl0 fs-6 @else align-self-center px-4 fs-5 cl2 @endif">
            @if (session('nav') == 'dashboard')
                <p class="fs-7 m-0 text-decoration-none">Dashboard</p>
            @endif
        </a>
        <a href="/money"
            class="py-2 btn bi bi-wallet fs-5 @if (session('nav') == 'money') cl0 fs-6 @else align-self-center px-4 fs-5 cl2 @endif">
            @if (session('nav') == 'money')
                <p class="fs-7 m-0 text-decoration-none">Money</p>
            @endif
        </a>
        <a href="/todo"
            class="py-2 btn bi bi-card-list fs-5 @if (session('nav') == 'todo') cl0 fs-6 @else align-self-center px-4 fs-5 cl2 @endif">
            @if (session('nav') == 'todo')
                <p class="fs-7 m-0 text-decoration-none">Todo</p>
            @endif
        </a>
        <a href="/whenit"
            class="py-2 btn bi bi-calendar-date fs-5 @if (session('nav') == 'whenit') cl0 fs-6 @else align-self-center px-4 fs-5 cl2 @endif">
            @if (session('nav') == 'whenit')
                <p class="fs-7 m-0 text-decoration-none">WhenIt</p>
            @endif
        </a>
    </nav>

    <main class="container py-5 px-0 ps-lg-5 ms-lg-5">
        <div class="d-flex justify-content-end d-lg-none">
            <div class="d-flex justify-content-between px-4 r40 py-3 text-center bg1 align-self-center mb-5">
                <p class="clamp1 px-2 wht8 f700 fs-6 m-0 align-self-center">{{ session('name') }}</p>
                <div class="ps-2">
                    <img src="{{ session('picture') }}" class="rounded-circle" style="width: 2rem" alt="">
                    <a class="px-2 cl2 fs-3 me-3" href="/logout" onclick="return confirm('Are you sure to Logout?')">
                        <span class="position-absolute bi bi-box-arrow-right" style="padding-top: .2rem"></span>
                    </a>
                </div>
            </div>
        </div>
        @if (session('notification') != null)
            <div class="position-fixed f600 alert ps-4 r20 alert-dismissible fade show @if (session('status')) alert-info @else alert-danger @endif"
                role="alert" style="right: 5vw; top:5vh" id="globalNotification">
                {{ session('msg') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        <h1 class="f600 d-none d-lg-block mb-5 wht9 ms-2 ps-5">@yield('title')</h1>
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="px-5">
                    @yield('left')
                </div>
                @yield('left1')
            </div>
            <div class="col-12 col-md-6 ps-md-5">
                @yield('right')
            </div>
            <div class="col-12 pt-4">
                @yield('bottom')
            </div>
        </div>
    </main>
    @yield('modal')
</body>
<script>
    setTimeout(() => {
        $("#globalNotification").fadeOut(500);
    }, 3000);
</script>

</html>
