@extends('layouts.template')

@section('tabTitle', 'Money')
@section('title', 'Money Flow')

@section('left')
    <section>
        <div class="d-flex justify-content-between">
            <div class="">
                <p class="f600 fs-6 m-0 wht9">Today Earnings</p>
                <h6 class="f600 fs-3 cl0">@rupiah($earn)</h6>
            </div>
            <div class="">
                <p class="f600 fs-6 m-0 wht9">Today Spendings</p>
                <h6 class="f600 fs-3 cl1">@rupiah($spend)</h6>
            </div>
        </div>
    </section>
@endsection

@section('left1')
    <x-card :shadow="true" style="primary">
        <h2 class="ps-4 py-2 mb-4 fs-4 wht9 f600">New Transaction</h2>
        <form action="/money" method="POST">
            @csrf
            <div>
                <div class="row mb-2">
                    <div class="col-12 col-lg-7">
                        <div class="r20 d-flex bg2 p-3">
                            <label for="money" class="align-self-center px-3 fs-6 wht9 f600">Rp</label>
                            <input type="number" id="money" style="outline:none" min="0"
                                class="border-0 bg-transparent fs-5 wht9 f600 w-100" placeholder="0" name="amount" required>
                        </div>
                    </div>
                    <div class="col-12 col-lg-5 mt-4 mt-lg-0">
                        <input type="hidden" name="type" id="type" value="out" required>
                        <div class="d-flex justify-content-between">
                            <button type="button" id="typeOut" class="f600 bg2 w-50 border-0 r20 px-0 cl1 py-3"
                                onclick="setType('out', '')">out</button>
                            <button type="button" id="typeIn" class="f600 bg-transparent border-0 w-50 px-0 r20 cl0 py-3"
                                onclick="setType('in', '')">in</button>
                        </div>
                    </div>
                </div>
                <textarea class="border-0 bg2 r20 mt-3 p-4 fs-6 wht8 f600 w-100" style="outline:none" rows="3" placeholder="Description"
                    name="desc"></textarea>
                <div class="r20 d-flex bg2 p-3 mb-4 mt-3">
                    <label for="money" class="align-self-center px-3 fs-6 wht6 f600">Date</label>
                    <input type="date" id="date" style="outline:none" class="border-0 bg-transparent fs-6 wht8 f600 w-100"
                        placeholder="date" name="date" value="{{ $today }}" required>
                </div>
                <div class="d-flex mb-3 pt-3">
                    <button type="submit" class="f600 bg2 shdw border-0 r20 wht9 px-5 py-3 d-lg-block d-none">Add
                        Transaction</button>
                    <button type="submit" class="f600 bg2 shdw border-0 r20 wht9 px-5 py-3 d-block d-lg-none">Add</button>
                    <button type="reset" class="f600 bg-transparent border-0 r20 wht6 px-4 py-3">Clear</button>
                </div>
            </div>
        </form>
    </x-card>

    <section class="pt-5 ">
        <h2 class="ps-5 py-2 mb-4 fs-4 wht9 f600">Transaction History</h2>
        @forelse ($transactions as $tr)
            <div data-bs-toggle="modal" data-bs-target="#moneyModal"
                onclick="setData({{ Illuminate\Support\Js::from($tr) }})">
                <x-card :shadow="true" style="secondary" class="my-4">
                    <div class="px-3 row">
                        <div class="col-7 d-flex">
                            <span
                                class="align-self-center bi @if ($tr->type == 'in') bi-arrow-down-circle cl0 @endif  @if ($tr->type == 'out') bi-arrow-up-circle cl1 @endif fs-2"></span>
                            <div class="align-self-center ps-3">
                                <p class="f600 fs-6 wht9 mb-0 clamp1">{{ $tr->desc }}</p>
                                <p class="f600 fs-7 wht6 mb-0 lt0">{{ $tr->date }}</p>
                            </div>
                        </div>
                        <div class="col-5 align-self-center text-end">
                            <p class="m-0 f600 fs-5 wht9">@rupiah($tr->amount)</p>
                        </div>
                    </div>
                </x-card>
            </div>
        @empty
            <x-card class="mb-5" :shadow="true" style="secondary">
                <h2 class="py-5 text-center m-0 fs-6 wht9 f600">No Transactions</h2>
            </x-card>
        @endforelse
    </section>
@endsection

@section('right')
    <section class="px-3">
        <p class="f600 fs-6 wht8 ps-2">This Month</p>
        <div class="d-flex">
            <img src="{{ asset('img/in.png') }}" alt="">
            <div class="ps-4 w-100">
                <div class="d-flex justify-content-between">
                    <p class="f600 fs-6 wht9">Earnings</p>
                    <H6 class="f600 fs-5 wht9">@rupiah($Mearn)</H6>
                </div>
                @if ($total == 0)
                    <x-progress color="#92DCFE" :percentage="0"></x-progress>
                @else
                    <x-progress color="#92DCFE" :percentage="($Mearn / $total) * 100"></x-progress>
                @endif
            </div>
        </div>
        <br>
        <div class="d-flex">
            <img src="{{ asset('img/out.png') }}" alt="">
            <div class="ps-4 w-100">
                <div class="d-flex justify-content-between">
                    <p class="f600 fs-6 wht9">Spendings</p>
                    <H6 class="f600 fs-5 wht9">@rupiah($Mspend)</H6>
                </div>
                @if ($total == 0)
                    <x-progress color="#ED5333" :percentage="0"></x-progress>
                @else
                    <x-progress color="#ED5333" :percentage="($Mspend / $total) * 100"></x-progress>
                @endif
            </div>
        </div>
        <br>
    </section>
    <section class="my-lg-4 p-2 bg2 r40 shdw">
        <div class="p-4">
            <div class="d-flex justify-content-between">
                <p class="f600 fs-6 wht6">Wishlist Total</p>
            </div>
            <h2 class="f600 wht9">@rupiah($totalWl)</h2>
        </div>
    </section>
    <section class="my-lg-4 pt-3">
        <div class="d-flex justify-content-between px-4">
            <span class="align-self-center f600 fs-6 ms-2 wht9">Wishlist Item</span>
            <span class="align-self-center bi bi-plus wht9 fs-4" data-bs-toggle="modal"
                data-bs-target="#wishlistFormModal"></span>
            <div class="modal fade" id="wishlistFormModal" tabindex="-1" aria-labelledby="wishlistFormModalLabel"
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content" style="background:transparent;">
                        <x-card :shadow="true" style="primary">
                            <div class="d-flex justify-content-between mb-4">
                                <h5 class="ps-4 py-2 fs-5 wht9 f600 modal-title">Add New Wishlist</h5>
                                <span class="bi bi-x-lg align-self-center wht8 px-3" data-bs-dismiss="modal"></span>
                            </div>
                            <form action="/wishlist" id="updateForm" method="POST">
                                @csrf
                                <input type="text" style="outline:none"
                                    class="border-0 r20 bg2 px-4 py-3 me-4 mb-2 fs-5 wht9 f600 w-100" placeholder="Item"
                                    name="item" id="vItem" required>
                                <div class="r20 d-flex bg2 p-3 my-4">
                                    <label for="price" class="align-self-center px-3 fs-6 wht9 f600">Rp</label>
                                    <input type="number" id="price" style="outline:none"
                                        class="border-0 bg-transparent fs-5 wht9 f600 w-100" placeholder="Price"
                                        name="price" id="vPrice" required>
                                </div>
                                <div class="d-flex mb-3 pt-2">
                                    <button type="submit"
                                        class="f600 d-none d-md-block shdw bg2 border-0 r20 wht9 px-5 py-3 ">Add
                                        Wishlist</button>
                                    <button type="submit"
                                        class="f600 d-md-none d-block shdw bg2 border-0 r20 wht9 px-5 py-3 ">Add</button>
                                    <button type="reset"
                                        class="f600 bg-transparent border-0 r20 wht6 px-4 py-3">Clear</button>
                                </div>
                            </form>
                        </x-card>
                    </div>
                </div>
            </div>
        </div>
        @forelse ($wishlist as $wl)
            <div data-bs-toggle="modal" data-bs-target="#wishlistModal"
                onclick="setDataWL({{ Illuminate\Support\Js::from($wl) }})">
                <x-card :shadow="false" style="secondary">
                    <div class="d-flex justify-content-between py-1 px-2">
                        <p class="align-self-center m-0 f600 fs-6 wht6">{{ $wl->item }}</p>
                        <p class="align-self-center m-0 f600 fs-6 wht6">@rupiah($wl->price)</p>
                    </div>
                </x-card>
            </div>
        @empty
            <x-card class="mb-5" :shadow="true" style="secondary">
                <h2 class="py-5 text-center m-0 fs-6 wht9 f600">No Items</h2>
            </x-card>
        @endforelse
    </section>
@endsection

@section('modal')
    <div class="modal fade" id="moneyModal" tabindex="-1" aria-labelledby="moneyModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="background:transparent;">
                <x-card :shadow="true" style="primary">
                    <div class="d-flex justify-content-between mb-4">
                        <div class="d-flex">
                            <h5 class="ps-4 py-2 fs-5 wht9 f600 modal-title">Transaction Detail</h5>
                        </div>
                        <form action="/money/destroy" id="moneydeleteForm" method="POST">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="align-self-center btn m-0 p-2 cl1 f600 fs-6"
                                onclick="return confirm('Are you sure to delete this event ?')">Delete</button>
                        </form>
                    </div>
                    <form action="/money" id="moneyupdateForm" method="POST">
                        @csrf
                        @method('PATCH')
                        <div>
                            <div class="row mb-2">
                                <div class="col-12 col-lg-7">
                                    <div class="r20 d-flex bg2 p-3">
                                        <label for="money" class="align-self-center px-3 fs-6 wht9 f600">Rp</label>
                                        <input type="number" id="modalMoney" style="outline:none" min="0"
                                            class="border-0 bg-transparent fs-5 wht9 f600 w-100" placeholder="0"
                                            name="amount" required>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-5 mt-4 mt-lg-0">
                                    <input type="hidden" name="type" id="modalType" value="out" required>
                                    <div class="d-flex justify-content-between">
                                        <button type="button" id="modaltypeOut"
                                            class="f600 bg2 w-50 border-0 r20 px-0 cl1 py-3"
                                            onclick="setType('out', 'modal')">out</button>
                                        <button type="button" id="modaltypeIn"
                                            class="f600 bg-transparent border-0 w-50 px-0 r20 cl0 py-3"
                                            onclick="setType('in', 'modal')">in</button>
                                    </div>
                                </div>
                            </div>
                            <textarea class="border-0 bg2 r20 my-4 p-4 fs-6 wht8 f600 w-100" style="outline:none" rows="3" placeholder="Description"
                                name="desc" id="modalDesc"></textarea>
                            <div class="r20 d-flex bg2 p-3 mb-4 ">
                                <label for="money" class="align-self-center px-3 fs-6 wht8 f600">Date</label>
                                <input type="date" id="modalDate" style="outline:none"
                                    class="border-0 bg-transparent fs-5 wht8 f600 w-100" placeholder="date" name="date"
                                    required>
                            </div>
                            <div class="d-flex mb-3">
                                <button type="submit"
                                    class="f600 d-none d-md-block bg2 shdw border-0 r20 wht9 px-5 py-3 ">Update
                                    Transaction</button>
                                <button type="submit"
                                    class="f600 d-md-none d-block bg2 shdw border-0 r20 wht9 px-5 py-3 ">Update</button>
                                <button type="button" data-bs-dismiss="modal"
                                    class="f600 bg-transparent border-0 r20 wht6 px-4 py-3">Cancel</button>
                            </div>
                        </div>
                    </form>
                </x-card>
            </div>
        </div>
    </div>

    <div class="modal fade" id="wishlistModal" tabindex="-1" aria-labelledby="wishlistModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="background:transparent;">
                <x-card :shadow="true" style="primary">
                    <div class="d-flex justify-content-between mb-4">
                        <div class="d-flex">
                            <h5 class="ps-4 py-2 fs-5 wht9 f600 modal-title">Item Detail</h5>
                        </div>
                        <form action="/wishlist/destroy" id="wishlistdeleteForm" method="POST">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="align-self-center btn m-0 p-2 cl1 f600 fs-6"
                                onclick="return confirm('Are you sure to delete this event ?')">Delete</button>
                        </form>
                    </div>
                    <form action="/wishlist" id="wishlistupdateForm" method="POST">
                        @csrf
                        @method('PATCH')
                        <input type="text" style="outline:none"
                            class="border-0 r20 bg2 px-4 py-3 me-4 mb-2 fs-5 wht9 f600 w-100" placeholder="Item"
                            name="item" id="vmodalItem" required>
                        <div class="r20 d-flex bg2 p-3 my-4">
                            <label for="vmodalPrice" class="align-self-center px-3 fs-6 wht9 f600">Rp</label>
                            <input type="number" style="outline:none" class="border-0 bg-transparent fs-5 wht9 f600 w-100"
                                placeholder="Price" name="price" id="vmodalPrice" required>
                        </div>
                        <div class="d-flex mb-3 pt-2">
                            <button type="submit"
                                class="f600 d-none d-md-block shdw bg2 border-0 r20 wht9 px-5 py-3 ">Update
                                Wishlist</button>
                            <button type="submit"
                                class="f600 d-md-none d-block shdw bg2 border-0 r20 wht9 px-5 py-3 ">Update
                            </button>
                            <button type="button" class="f600 bg-transparent border-0 r20 wht6 px-4 py-3"
                                data-bs-dismiss="modal">Cancel</button>
                        </div>
                    </form>
                </x-card>
            </div>
        </div>
    </div>
@endsection

@section('bottom')
    <script>
        const setType = (type, status) => {
            if (status == 'modal') {
                $('#modalType').val(type);
                $('#modaltypeIn').removeClass("bg2")
                $('#modaltypeOut').removeClass("bg2")
                $('#modaltypeIn').addClass("bg-transparent")
                $('#modaltypeOut').addClass("bg-transparent")
                if (type == 'in') {
                    $('#modaltypeIn').removeClass("bg-transparent")
                    $('#modaltypeIn').addClass("bg2")
                }
                if (type == 'out') {
                    $('#modaltypeOut').removeClass("bg-transparent")
                    $('#modaltypeOut').addClass("bg2")
                }
            } else {
                $('#type').val(type);
                $('#typeIn').removeClass("bg2")
                $('#typeOut').removeClass("bg2")
                $('#typeIn').addClass("bg-transparent")
                $('#typeOut').addClass("bg-transparent")
                if (type == 'in') {
                    $('#typeIn').removeClass("bg-transparent")
                    $('#typeIn').addClass("bg2")
                }
                if (type == 'out') {
                    $('#typeOut').removeClass("bg-transparent")
                    $('#typeOut').addClass("bg2")
                }
            }
        }
        const setData = (data) => {
            $('#modalMoney').val(data.amount);
            setType(data.type, 'modal')
            $('#modalDesc').val(data.desc);
            $('#modalDate').val(data.date);
            $("#moneyupdateForm").attr('action', '/money/' + data.id);
            $("#moneydeleteForm").attr('action', '/money/' + data.id);
        }
        const setDataWL = (data) => {
            $('#vmodalItem').val(data.item);
            $('#vmodalPrice').val(data.price);
            $("#wishlistupdateForm").attr('action', '/wishlist/' + data.id);
            $("#wishlistdeleteForm").attr('action', '/wishlist/' + data.id);
        }
    </script>
@endsection
