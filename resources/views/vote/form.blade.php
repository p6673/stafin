<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="{{ asset('js/bootstrap.bundle.js') }}"></script>
    <title>Vote Form</title>
    <link rel="stylesheet" href="{{ asset('css/all.css') }}">
    <link rel="icon" type="image/x-icon" href="{{ asset('icon/favicon.png') }}">
</head>

<body class="bg0 d-flex justify-content-center">
    <div class="py-5" style="width: clamp(20rem,93vw,55rem)">
        <x-card :shadow="true" style="primary">
            <div class="p-md-4">
                @if (session('notification') != null)
                    <h1 class="f600 wht9 pb-5 text-center">WhenIt</h1>
                    <h4 class="f600 wht9 pb-5 text-center">Your Vote has been Added Successfully, Thank You for Your Vote
                    </h4>
                @elseif ($id->status)
                    <h1 class="f700 wht9 pb-5 text-center">Vote Completed</h1>
                    <h1 class="fs-5 wht9 f700">{{ $id->title }}</h1>
                    <h6 class="fs-7 wht8 f700">This vote is about <i>{{ $id->desc }}</i></h6>
                    <h6 class="fs-7 wht8 f700">The summary date is <span
                            class="wht9">{{ $id->summary }}</span></h6>
                @else
                    <h1 class="f600 wht9 pb-5 text-center">WhenIt</h1>
                    <h1 class="fs-5 wht9 f600">{{ $id->title }}</h1>
                    <h6 class="fs-7 wht6 f600">This vote is about <i>{{ $id->desc }}</i></h6>
                    <h6 class="fs-7 wht6 f600">Start From <span class="wht9">{{ $id->start }}</span> to
                        <span class="wht9">{{ $id->end }}</span>
                    </h6>
                    <br>
                    <br>
                    <form action="/vote/{{ $id->id }}/form" method="POST">
                        @csrf
                        <div class="r20 d-flex bg2 p-3 shdw mb-5">
                            <label for="inName" class="align-self-center px-3 fs-6 wht6 f600">Name</label>
                            <input type="text" id="inName" style="outline:none"
                                class="border-0 bg-transparent fs-5 wht8 f600 w-100" name="name" required>
                        </div>
                        @for ($a = 0; $a < count($id->summary); $a++)
                            <x-card :shadow="true" style="secondary" class="my-4">
                                <div class="py-2">
                                    <div class="d-flex d-lg-inline flex-column flex-lg-row">
                                        <span class="align-self-center w-100">
                                            <button type="button" class="fs-6 wht8 p-0 border-0 bg-transparent f600"
                                                onclick="allDay({{ $a }})"
                                                id="sum{{ $a }}">{{ $id->summary[$a] }}</button>
                                            <span class="fs-6 wht6 f600 float-end d-block d-lg-none"
                                                id="clr{{ $a }}" style="cursor:pointer"
                                                onclick="clr({{ $a }})">Clear</span>
                                        </span>
                                        <span
                                            class="align-self-center w-100 d-flex d-lg-inline flex-column flex-md-row justify-content-between">
                                            <span>
                                                <span class="fs-6 wht6 f600 ps-lg-5">From</span>
                                                <input type="time"
                                                    class="f600 float-end float-md-none bg2 border-0 r20 wht6 px-md-4 w-auto"
                                                    name="start{{ $a }}" id="start{{ $a }}"
                                                    onchange="swap({{ $a }})" placeholder="time Start">
                                            </span>
                                            <span class="">
                                                <span class="fs-6 wht6 f600">To</span>
                                                <input type="time"
                                                    class="f600 float-end float-md-none bg2 border-0 r20 wht6 ps-md-4 px-lg-4 w-auto"
                                                    name="end{{ $a }}" id="end{{ $a }}"
                                                    onchange="swap({{ $a }})" placeholder="time End">
                                            </span>
                                        </span>
                                        <span class="align-self-center d-none d-lg-inline w-100 pe-lg-2">
                                            <span class="fs-6 wht6 f600 float-end" id="clr{{ $a }}"
                                                style="cursor:pointer" onclick="clr({{ $a }})">Clear</span>
                                        </span>
                                    </div>
                                </div>
                            </x-card>
                        @endfor
                        <br>
                        <button type="submit" class="f600 shdw bg2 border-0 r20 cl0 w-100 py-3">Submit</button>
                    </form>
                @endif
            </div>
        </x-card>
    </div>
    <script>
        const allDay = (id) => {
            $('#start' + id).val('00:01')
            $('#end' + id).val('23:59')
            swap(id)
        }
        const swap = (id) => {
            if ($('#start' + id).val() != "" && $('#end' + id).val() != "") {
                $('#sum' + id).removeClass('wht8');
                $('#sum' + id).addClass('cl0');
                $('#start' + id).removeClass('wht6');
                $('#start' + id).addClass('cl0');
                $('#end' + id).removeClass('wht6');
                $('#end' + id).addClass('cl0');
                $('#clr' + id).removeClass('wht6');
                $('#clr' + id).addClass('cl1');
                if ($('#start' + id).val() > $('#end' + id).val()) {
                    let temp = $('#start' + id).val();
                    $('#start' + id).val($('#end' + id).val())
                    $('#end' + id).val(temp)
                }
            } else {
                $('#sum' + id).removeClass('cl0');
                $('#sum' + id).addClass('wht8');
                $('#start' + id).removeClass('cl0');
                $('#start' + id).addClass('wht6');
                $('#end' + id).removeClass('cl0');
                $('#end' + id).addClass('wht6');
                $('#clr' + id).removeClass('cl1');
                $('#clr' + id).addClass('wht6');
            }
        }
        const clr = (id) => {
            $('#start' + id).prop('disabled', true);
            $('#start' + id).val('');
            $('#start' + id).prop('disabled', false);
            $('#end' + id).prop('disabled', true);
            $('#end' + id).val('');
            $('#end' + id).prop('disabled', false);
            swap(id);
        }
    </script>
</body>

</html>
