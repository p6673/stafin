<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="{{ asset('js/bootstrap.bundle.js') }}"></script>
    <title>Vote</title>
    <link rel="stylesheet" href="{{ asset('css/all.css') }}">
    <link rel="icon" type="image/x-icon" href="{{ asset('icon/favicon.png') }}">
</head>

<body class="bg0">
    <div class="container my-5 px-3">
        <div class="d-flex justify-content-between py-5 mb-lg-5">
            <div class="align-self-center">
                <a href="/whenit" class="bg2 px-3py-2 btn r20 bi bi-chevron-left wht8"><span
                        class="d-none d-lg-inline pe-2">&nbsp; back to WhenIt</span></a>
            </div>
            <div class="d-flex justify-content-center align-self-center">
                <h1 class="f600 wht9 text-center m-0">Vote Dashboard</h1>
                @if ($id->status)
                    <span class="cl0 bg2 py-2 px-3 r20 fs-7 align-self-center d-none d-lg-inline"
                        style="letter-spacing: 0.04em;">completed</span>
                @else
                    <span class="cl1 bg2 py-2 px-3 r20 fs-7 align-self-center d-none d-lg-inline"
                        style="letter-spacing: 0.04em;">uncompleted</span>
                @endif
            </div>
            <span>
                <div class="@if ($id->status)  @endif align-self-center d-none d-lg-flex">
                    @if (!$id->status)
                        <a href="/vote/{{ $id->id }}/form" target="blank"
                            class="btn align-self-center me-3 py-2 bg2 r20 px-4 wht8 f600">Form</a>
                    @else
                        <form action="/whenit/{{ $id->id }}" method="POST">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="align-self-center btn m-0 p-2 cl1 f700 fs-6"
                                onclick="return confirm('Are you sure to delete this event ?')">Delete</button>
                        </form>
                        @if ($id->status == 2)
                            <form action="/whenit/{{ $id->id }}/unarchive" method="POST">
                                @method('PATCH')
                                @csrf
                                <button type="submit"
                                    class="align-self-center f600 ms-3 bg2 border-0 r20 wht8 px-3 py-2"
                                    onclick="return confirm('Are you sure to unarchive this event ?')">Unarchive</button>
                            </form>
                        @else
                            <form action="/whenit/{{ $id->id }}/archive" method="POST">
                                @method('PATCH')
                                @csrf
                                <button type="submit"
                                    class="align-self-center f600 ms-3 bg2 border-0 r20 wht8 px-3 py-2"
                                    onclick="return confirm('Are you sure to archive this event ?')">Archive</button>
                            </form>
                        @endif
                    @endif
                </div>
            </span>
        </div>
        <div class="d-flex justify-content-center ps-4 pb-5 d-lg-none">
            @if ($id->status)
                <span class="cl0 bg2 py-2 px-3 r20 fs-7 align-self-center"
                    style="letter-spacing: 0.04em;">completed</span>
            @else
                <span class="cl1 bg2 py-2 px-3 r20 fs-7 align-self-center"
                    style="letter-spacing: 0.04em;">uncompleted</span>
            @endif

            <span class="d-flex ms-3">
                @if (!$id->status)
                    <a href="/vote/{{ $id->id }}/form" target="blank"
                        class="btn align-self-center me-3 py-2 bg2 r20 px-4 wht8 f600">Form</a>
                @else
                    <form action="/whenit/{{ $id->id }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="align-self-center btn m-0 p-2 cl1 f700 fs-6"
                            onclick="return confirm('Are you sure to delete this event ?')">Delete</button>
                    </form>
                    @if ($id->status == 2)
                        <form action="/whenit/{{ $id->id }}/unarchive" method="POST">
                            @method('PATCH')
                            @csrf
                            <button type="submit" class="align-self-center f600 ms-3 bg2 border-0 r20 wht8 px-3 py-2"
                                onclick="return confirm('Are you sure to unarchive this event ?')">Unarchive</button>
                        </form>
                    @else
                        <form action="/whenit/{{ $id->id }}/archive" method="POST">
                            @method('PATCH')
                            @csrf
                            <button type="submit" class="align-self-center f600 ms-3 bg2 border-0 r20 wht8 px-3 py-2"
                                onclick="return confirm('Are you sure to archive this event ?')">Archive</button>
                        </form>
                    @endif
                @endif
            </span>
        </div>
        <div class="row">
            <div class="col-12 col-lg-6 pe-lg-5">
                <div class="mx-3 px-3 mb-5">
                    <h1 class="fs-5 wht9 f600">Event : {{ $id->title }}</h1>
                    <h6 class="fs-7 wht6 f600">Description : {{ $id->desc }}</h6>
                    <h6 class="fs-7 wht6 f600">Start From <span class="wht9">{{ $id->start }}</span> to
                        <span class="wht9">{{ $id->end }}</span>
                    </h6>
                </div>
                @forelse ($event as $ev)
                    @if ($id->status && date('Y-m-d', strtotime($id->summary)) == $ev->vote)
                        <x-card :shadow="true" style="primary" class="mb-4">
                            <div class="d-flex justify-content-between">
                                <p class="f600 cl0 fs-5 m-0">
                                    {{ $ev->vote }}</p>
                                <p class="f600 wht8 fs7 m-0">{{ $ev->voted }} participans</p>
                            </div>
                            <p class="f600 wht8 fs-7">Avg Duration : {{ round($ev->dur, 2) }} hours</p>
                            <div class="d-flex justify-content-between">
                                <p class="cl0 fs-5 m-0">
                                    {{ $summary[$ev->vote]['start'] . ' to ' . $summary[$ev->vote]['end'] }} </p>
                                <span class="m-0 py-2 bg2 r20 px-4 wht8 f600">Selected</span>
                            </div>
                        </x-card>
                        @continue
                    @endif
                    <x-card :shadow="true" style="primary" class="mb-4">
                        <div class="d-flex justify-content-between">
                            <p class="f600 @if ($id->status) wht6 @else cl0 @endif fs-6 m-0">
                                {{ $ev->vote }}</p>
                            <p class="f600 wht6 fs7 m-0">{{ $ev->voted }} participans</p>
                        </div>
                        <p class="f600 wht6 fs-7">Avg Duration : {{ round($ev->dur, 2) }} hours</p>
                        <div class="d-flex justify-content-between">
                            <p class="@if ($id->status) wht6 @else wht9 @endif fs-5 m-0">
                                {{ $summary[$ev->vote]['start'] . ' to ' . $summary[$ev->vote]['end'] }} </p>
                            @if (!$id->status)
                                <a href="/vote/{{ $id->id }}/summary/{{ $ev->vote }}/{{ $summary[$ev->vote]['start'] }}"
                                    class="btn m-0 py-1 bg2 r20 px-3 wht8 f600">Select</a>
                            @endif
                        </div>
                    </x-card>
                @empty
                    <x-card class="mb-5" :shadow="true" style="secondary">
                        <h2 class="py-5 text-center m-0 fs-6 wht9 f600">No Vote</h2>
                    </x-card>
                @endforelse
            </div>
            <div class="col-12 col-lg-6 mt-5 mt-lg-0 ps-lg-5">
                <p class="f600 wht9 fs-5">Participans</p>
                @forelse ($participans as $p)
                    <div class="d-flex justify-content-between py-3">
                        <div class="w-100">
                            <div class="row">
                                <p class="f600 col-7 wht8 fs-6 clamp1">{{ $p->name }}</p>
                                <p class="f600 col-5 text-end wht6 fs-6">{{ round($p->total, 2) }} hours</p>
                            </div>
                            <x-progress color="#92DCFE" :percentage="($p->total / $max) * 100"></x-progress>
                        </div>
                        @if (!$id->status)
                            <a href="/vote/{{ $id->id }}/hide/{{ $p->inputId }}"
                                class="f600 wht6 fs-6 align-self-center btn bg2 ms-4 r20">hide</a>
                        @endif
                    </div>
                @empty
                    <x-card class="mb-5" :shadow="true" style="secondary">
                        <h2 class="py-5 text-center m-0 fs-6 wht9 f600">No Participans</h2>
                    </x-card>
                @endforelse

                <p class="f600 wht6 fs-6 mt-5">Hided Participans</p>
                @forelse ($hided as $hd)
                    <div class="d-flex justify-content-between pb-3">
                        <div class="w-100">
                            <div class="row">
                                <p class="f600 col-7 wht6 fs-6">{{ $hd->name }}</p>
                                <p class="f600 col-5 text-end wht6 fs-6">{{ round($hd->total, 2) }} hours</p>
                            </div>
                            <x-progress color="rgba(255, 255, 255, .6)" :percentage="($hd->total / $max) * 100"></x-progress>
                        </div>
                        @if (!$id->status)
                            <a href="/vote/{{ $id->id }}/show/{{ $hd->inputId }}"
                                class="f600 wht6 fs-6 align-self-center btn bg2 ms-4 r20">show</a>
                        @endif
                    </div>
                @empty
                    <x-card class="mb-5" :shadow="true" style="secondary">
                        <h2 class="py-5 text-center m-0 fs-6 wht9 f600">No Hided Participans</h2>
                    </x-card>
                @endforelse
            </div>
        </div>

    </div>
</body>

</html>
