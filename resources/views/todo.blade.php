@extends('layouts.template')

@section('tabTitle', 'To Do')
@section('title', 'To Do')

@section('left')
@endsection

@section('left1')
    <x-card :shadow="true" style="primary">
        <h2 class="ps-4 py-2 mb-4 fs-4 wht9 f600">New To Do</h2>
        <form action="/todo" method="POST">
            @csrf
            <div>
                <div class="d-flex mb-2">
                    <input type="text" style="outline:none" class="border-0 r20 bg2 px-4 py-3 fs-5 wht9 f600 w-100"
                        placeholder="Title" name="title" required>
                </div>
                <div class="row mt-4">
                    <div class="col-12 col-lg-6">
                        <input type="date" class="f600 me-4 bg2 border-0 r20 w-100 wht6 px-4 py-3" placeholder="Date"
                            name="date">
                    </div>
                    <div class="col-12 col-lg-6 mt-4 mt-lg-0">
                        <div class="r20 d-flex bg2 p-3 w-100">
                            <label for="time" class="align-self-center px-3 fs-6 wht6 f600">At</label>
                            <input type="time" id="time" style="outline:none"
                                class="border-0 bg-transparent fs-6 wht9 f600 w-100" placeholder="time" name="time">
                        </div>
                    </div>
                </div>
                <textarea class="border-0 bg2 r20 my-4 p-4 fs-6 wht8 f600 w-100" style="outline:none" rows="3" placeholder="Description"
                    name="desc"></textarea>
                <div class="d-flex mb-3">
                    <button type="submit" class="f600 d-none d-md-block shdw bg2 border-0 r20 wht9 px-5 py-3 ">Add To
                        Do</button>
                    <button type="submit" class="f600 d-md-none d-block shdw bg2 border-0 r20 wht9 px-5 py-3 ">Add</button>
                    <button type="reset" class="f600 bg-transparent border-0 r20 wht6 px-4 py-3 ">Clear</button>
                </div>
            </div>
        </form>
    </x-card>
    <section class="pt-5 ">
        <h2 class="ps-5 py-2 mb-4 fs-4 wht9 f600">Uncompleted Tasks</h2>
        @forelse ($todo as $td)
            <div data-bs-toggle="modal" data-bs-target="#todoModal"
                onclick="setData({{ Illuminate\Support\Js::from($td) }})">
                <x-card :shadow="true" style="secondary" class="my-4">
                    <div class="pe-3 d-flex justify-content-between">
                        <div class="d-flex">
                            <div class="align-self-center d-flex p-3 todo{{ $td->id }}"
                                onmouseenter="msin('{{ $td->id }}', {{ $td->status }})"
                                onmouseleave="msout('{{ $td->id }}', {{ $td->status }})"
                                onclick="checkTodo('{{ $td->id }}', {{ $td->status }})" style="z-index:100">
                                <div class="rounded-circle align-self-center" id="circle{{ $td->id }}"
                                    style="width:1.25rem; height:1.25rem; border:3px solid #ED5333">
                                </div>
                            </div>
                            <div class="align-self-center ps-2">
                                <p class="f600 fs-6 wht9 mb-0 ">
                                    {{ $td->title }}
                                </p>
                                <p class="f600 fs-7 wht6 mb-0 lt0">
                                    {{ $td->desc }}
                                </p>
                            </div>
                        </div>
                        <div class="align-self-center ps-2 text-end">
                            <p class="f600 fs-6 wht8 mb-0 ">
                                {{ $td->date }}
                            </p>
                            <p class="f600 fs-7 wht6 mb-0 lt0">
                                {{ $td->time }}
                            </p>
                        </div>
                    </div>
                </x-card>
            </div>
        @empty
            <x-card class="mb-5" :shadow="true" style="secondary">
                <h2 class="py-5 text-center m-0 fs-6 wht9 f600">No Tasks</h2>
            </x-card>
        @endforelse
    </section>
@endsection

@section('right')
    <section>
        <h2 class="ps-4 py-2 mb-4 fs-4 wht9 f600">Today's Tasks</h2>
        <section>
            @forelse ($todayTask as $tdTask)
                <div data-bs-toggle="modal" data-bs-target="#todoModal"
                    onclick="setData({{ Illuminate\Support\Js::from($tdTask) }})">
                    <x-card :shadow="true" style="secondary">
                        <div class="d-flex justify-content-between">
                            <div class="d-flex">
                                <div class="align-self-center d-flex p-3 todo{{ $tdTask->id }}"
                                    onmouseenter="msin('{{ $tdTask->id }}', {{ $tdTask->status }})"
                                    onmouseleave="msout('{{ $tdTask->id }}', {{ $tdTask->status }})"
                                    onclick="checkTodo('{{ $tdTask->id }}', {{ $tdTask->status }})">
                                    <div class="rounded-circle align-self-center" id="circle{{ $tdTask->id }}"
                                        style="width:1.25rem; height:1.25rem; border:3px solid #ED5333">
                                    </div>
                                </div>
                                <p class="align-self-center m-0 f600 fs-5 wht9">{{ $tdTask->title }}</p>
                            </div>
                            <p class="align-self-center m-0 f600 fs-6 wht8">{{ $tdTask->time }}</p>
                        </div>
                    </x-card>
                </div>
            @empty
                <x-card class="mb-5" :shadow="true" style="secondary">
                    <h2 class="py-5 text-center m-0 fs-6 wht9 f600">No Tasks Today</h2>
                </x-card>
            @endforelse
        </section>
        <p class="ps-4 pt-5 pb-0 fs-6 wht8 f600">Completed Tasks</p>
        <section>
            @forelse ($completed as $cp)
                <div data-bs-toggle="modal" data-bs-target="#todoModal"
                    onclick="setData({{ Illuminate\Support\Js::from($cp) }})">
                    <x-card :shadow="false" style="secondary">
                        <div class="d-flex justify-content-between pe-3">
                            <div class="d-flex">
                                <div class="align-self-center d-flex p-3 todo{{ $cp->id }}"
                                    onmouseenter="msin('{{ $cp->id }}', {{ $cp->status }})"
                                    onmouseleave="msout('{{ $cp->id }}', {{ $cp->status }})"
                                    onclick="checkTodo('{{ $cp->id }}', {{ $cp->status }})">
                                    <div class="rounded-circle align-self-center" id="circle{{ $cp->id }}"
                                        style="width:1.25rem; height:1.25rem; border:3px solid #92DCFE">
                                    </div>
                                </div>
                                <div class="align-self-center ps-2">
                                    <p class="f600 fs-6 wht9 mb-0 ">
                                        {{ $cp->title }}
                                    </p>
                                    <p class="f600 fs-7 wht6 mb-0 lt0">
                                        {{ $cp->desc }}
                                    </p>
                                </div>
                            </div>
                            <div class="align-self-center ps-2 text-end">
                                <p class="f600 fs-6 wht8 mb-0 ">
                                    {{ $cp->date }}
                                </p>
                                <p class="f600 fs-7 wht6 mb-0 lt0">
                                    {{ $cp->time }}
                                </p>
                            </div>
                        </div>
                    </x-card>
                </div>
            @empty
                <x-card class="mb-5" :shadow="true" style="secondary">
                    <h2 class="py-5 text-center m-0 fs-6 wht9 f600">No Completed Tasks</h2>
                </x-card>
            @endforelse
        </section>
    </section>
@endsection

@section('bottom')
    <form action="/todo/check/" method="POST" id="checkForm" style="display:none">
        @csrf
        @method('PATCH')
        <button type="submit" id="submitBtn">submit</button>
    </form>

    <script>
        const setData = (data) => {
            $('#modalTitle').val(data.title);
            $('#modalDesc').val(data.desc);
            $('#modalTime').val(data.time);
            $('#modalDate').val(data.date);
            $("#todoupdateForm").attr('action', '/todo/' + data.id);
            $("#tododeleteForm").attr('action', '/todo/' + data.id);
        }

        const msin = (id, status) => {
            if (status) $('.todo' + id).html(
                `<span class="bi bi-x-lg fs-4 align-self-center wht9" id="check${id}" style="cursor:pointer""></span>`
            );
            else $('.todo' + id).html(
                `<span class="bi bi-check-lg fs-4 align-self-center wht9" id="check${id}" style="cursor:pointer""></span>`
            )
        }
        const msout = (id, status) => {
            if (status) $('.todo' + id).html(
                `<div class="rounded-circle align-self-center" id="circle${id}" style="width:1.25rem; height:1.25rem; border:3px solid #92DCFE"></div>`
            );
            else $('.todo' + id).html(
                `<div class="rounded-circle align-self-center" id="circle${id}" style="width:1.25rem; height:1.25rem; border:3px solid #ED5333"></div>`
            );
        }

        const checkTodo = (id, status) => {
            if (status) $("#checkForm").attr('action', '/todo/' + id + '/uncheck');
            else $("#checkForm").attr('action', '/todo/' + id + '/check');
            $('#submitBtn').click();
        }
    </script>
@endsection

@section('modal')
    <div class="modal fade" id="todoModal" tabindex="-1" aria-labelledby="todoModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="background:transparent;">
                <x-card :shadow="true" style="primary">
                    <div class="d-flex justify-content-between mb-4">
                        <div class="d-flex">
                            <h5 class="ps-4 py-2 fs-5 wht9 f600 modal-title">To Do Detail</h5>
                        </div>
                        <form action="/todo/destroy" id="tododeleteForm" method="POST">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="align-self-center btn m-0 p-2 cl1 f600 fs-6"
                                onclick="return confirm('Are you sure to delete this event ?')">Delete</button>
                        </form>
                    </div>
                    <form action="/todo" id="todoupdateForm" method="POST">
                        @csrf
                        @method('PATCH')
                        <div>
                            <div class="d-flex mb-2">
                                <input type="text" style="outline:none"
                                    class="border-0 r20 bg2 px-4 py-3 fs-5 wht9 f600 w-100" placeholder="Title" name="title"
                                    id="modalTitle" required>
                            </div>
                            <div class="row mt-4">
                                <div class="col-12 col-lg-6">
                                    <input type="date" class="f600 me-4 bg2 border-0 r20 w-100 wht6 px-4 py-3"
                                        placeholder="Date" name="date" id="modalDate">
                                </div>
                                <div class="col-12 col-lg-6 mt-4 mt-lg-0">
                                    <div class="r20 d-flex bg2 p-3 w-100">
                                        <label for="modalTime" class="align-self-center px-3 fs-6 wht6 f600">At</label>
                                        <input type="time" id="modalTime" style="outline:none"
                                            class="border-0 bg-transparent fs-6 wht9 f600 w-100" placeholder="time"
                                            name="time">
                                    </div>
                                </div>
                            </div>
                            <textarea class="border-0 bg2 r20 my-4 p-4 fs-6 wht8 f600 w-100" style="outline:none" rows="3" placeholder="Description"
                                name="desc" id="modalDesc"></textarea>
                            <div class="d-flex mb-3">
                                <button type="submit"
                                    class="f600 d-none d-md-block shdw bg2 border-0 r20 wht9 px-5 py-3 ">Update To
                                    Do</button>
                                <button type="submit"
                                    class="f600 d-md-none d-block shdw bg2 border-0 r20 wht9 px-5 py-3 ">Update</button>
                                <button type="button" data-bs-dismiss="modal"
                                    class="f600 bg-transparent border-0 r20 wht6 px-4 py-3 ">Cancel</button>
                            </div>
                        </div>
                    </form>
                </x-card>
            </div>
        </div>
    </div>
@endsection
