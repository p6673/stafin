<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="{{ asset('js/bootstrap.bundle.js') }}"></script>
    <title>Login</title>
    <link rel="stylesheet" href="{{ asset('css/all.css') }}">
    <link rel="icon" type="image/x-icon" href="{{ asset('icon/favicon.png') }}">
</head>

<body class="bg0 container">
    <div style="width: clamp(300px, 50vw, 35rem)" class="m-auto my-5 py-5">
        @if (session('notification') != null)
            <div class="position-fixed f600 alert ps-4 r20 alert-dismissible fade show @if (session('status')) alert-info @else alert-danger @endif"
                role="alert" style="right: 100px" id="globalNotification">
                {{ session('msg') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        <x-card :shadow="true" style="primary" class="text-center pb-5 px-0">
            <div class="pt-3 pb-5 mb-5 d-flex flex-column align-items-center">
                <h2 class="f600 wht9">Stafin</h2>
                <h6 class="f600 wht6 mb-5">Welcome Back!</h6>
                <a href="auth/google" class="btn d-flex justify-content-between bg2 wht9 f600 r40 w-75 p-2 fs-5">
                    <div class="align-self-center">
                        <img src="{{ asset('icon/google.png') }}" style="width:2.5rem;" alt="">
                    </div>
                    <span class="align-self-center">Login with Google</span>
                    <span></span>
                </a>
            </div>
        </x-card>
    </div>
</body>
<script>
    setTimeout(() => {
        $("#globalNotification").fadeOut(500);
    }, 3000);
</script>

</html>
